package com.stock.mvc.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ManyToAny;

@Entity
@Table(name="mouvementStock")
public class MouvementStock implements Serializable {

	public static final int ENTREE =1;
	public static final int SORTIE =2;
	
	public Date getDateMouvementStock() {
		return dateMouvementStock;
	}

	public void setDateMouvementStock(Date dateMouvementStock) {
		this.dateMouvementStock = dateMouvementStock;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public BigDecimal getQuantite() {
		return quantite;
	}

	public void setQuantite(BigDecimal quantite) {
		this.quantite = quantite;
	}

	public int getTypeMvt() {
		return typeMvt;
	}

	public void setTypeMvt(int typeMvt) {
		this.typeMvt = typeMvt;
	}

	@Id
	@GeneratedValue
	private Long idMouvementStock;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateMouvementStock;
	
	
	@JoinColumn(name="idArticle")
	@ManyToOne
	private Article article;
	private BigDecimal quantite;
	private int typeMvt;

	public Long getIdMouvementStock() {
		return idMouvementStock;
	}

	public void setIdMouvementStock(Long idMouvementStock) {
		this.idMouvementStock = idMouvementStock;
	}
	
	
}
